package a22;

public interface Duck {
    public void quack();
    public void fly();
}