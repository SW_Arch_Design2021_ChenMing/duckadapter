package a22;

public class WildTurkey implements Turkey {
    public void gobble(){
        System.out.println("Gobble!");
    }

    public void fly(){
        System.out.println("I fly a short distance!");
    }
}