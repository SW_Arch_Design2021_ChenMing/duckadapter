package a22;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Duck MyDuck = new MallardDuck();
        Turkey MyTurkey = new WildTurkey();
        Duck MyTurkeyAdapter = new TurkeyAdapter(MyTurkey);

        MyDuck.quack();
        MyDuck.fly();
        MyTurkeyAdapter.quack();
        MyTurkeyAdapter.fly();
    }
}
